// hamburger menu toggle
let ham_trigger = document.querySelector('.p-button-box-04');
let ham_menu = document.querySelector('.c-button-ham');
let drawer = document.querySelector('.c-nav__wrapper');

ham_trigger.addEventListener('click',function () {
  if(ham_menu.className == 'c-button-ham c-button-ham--on'){
    ham_menu.classList.remove('c-button-ham--on');
    drawer.classList.add('u-tab-hide');
  }else{
    ham_menu.classList.add('c-button-ham--on');
    drawer.classList.remove('u-tab-hide');
  }
});

// slider function
let slider_items = document.getElementsByClassName('c-slider__item');
let index = 0;
slider_items[index].style.opacity = 1;

function slider() {
  slider_items[index].style.cssText = "animation: FadeOut 5s";
  if (index >= slider_items.length-1) {
    index = 0;
  } else{
    index += 1;
  }
  slider_items[index].style.cssText = "animation: FadeIn 5s";
}

setInterval(slider,5000)

