# test

- HTML5
- SCSS
- javascript

**実行環境の構築方法や確認方法**
WEB pack managerを使用していません。
リポジトリーをxampp(MAMP)のドキュメントディレクトリーの直下においてvhostsに追加すれば確認できるようになっています。

### Branch
- master

### 識別子の種類
- fix：バグの修正
- add：新規機能追加・ファイルの追加
- update：バグではない機能修正
- remove：削除（ファイル）


## CSS

FLOCSSをベースとしたCSS設計を使用します。FLOCSSについては以下参照。
[FLOCSS](https://github.com/hiloki/flocss)

- setting = mixin、変数 ...
- foundation = ベース ...
- layout = ページのレイアウトを構成する共通要素（ヘッダー、サイドナビ ...）
- object > component = 最小単位のパーツ（ボタン、タイトル ...）
- object > project = componentを含むもしくはそれ以外の要素で構成されたサイト内で使い回しが効くパーツ
- object > utility = ユーティリティクラス

